package com.example.pipe.jorgecastillo_prueba1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by pipe on 23-09-17.
 */

public class ListadoDeHorasTomadasAdapter extends ArrayAdapter<Hora>{

    private final List<Hora> list;
    private final Context context;

    public ListadoDeHorasTomadasAdapter(Context context, List<Hora> list) {
        super(context, R.layout.item_horas, list);
        this.context = context;
        this.list = list;
    }

    static class ViewHolder {
        protected TextView nombre, fecha, hora;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflator = LayoutInflater.from(context);
            convertView = inflator.inflate(R.layout.item_horas, null);
            holder = new ViewHolder();
            holder.nombre = (TextView) convertView.findViewById(R.id.lblNombre);
            holder.fecha = (TextView) convertView.findViewById(R.id.lblFecha);
            holder.hora = (TextView) convertView.findViewById(R.id.lblHora);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        Hora hora = getItem(position);

        holder.nombre.setText(hora.getNombre());
        holder.fecha.setText(hora.getFecha());
        holder.hora.setText(hora.getHora());

        return convertView;
    }
}
