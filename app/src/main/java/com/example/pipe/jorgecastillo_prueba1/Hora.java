package com.example.pipe.jorgecastillo_prueba1;

import java.util.Date;

/**
 * Created by pipe on 23-09-17.
 */

public class Hora {

    private String fecha,nombre,telefono, hora;

    public Hora(String fecha, String nombre, String telefono, String hora) {
        this.fecha = fecha;
        this.nombre = nombre;
        this.telefono = telefono;
        this.hora = hora;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
}
