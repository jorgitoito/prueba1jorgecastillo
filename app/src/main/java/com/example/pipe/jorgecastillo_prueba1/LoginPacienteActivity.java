package com.example.pipe.jorgecastillo_prueba1;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginPacienteActivity extends AppCompatActivity {

    private Button btnLogin, btnCallCenter;
    private EditText edUser, edPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {
        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnCallCenter = (Button)findViewById(R.id.btnCallCenter);
        edUser = (EditText) findViewById(R.id.edUsuario);
        edPass = (EditText) findViewById(R.id.edClave);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
        btnCallCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent x = new Intent(Intent.ACTION_DIAL);
                x.setData(Uri.parse("tel:+569 555-55-55"));
                startActivity(x);
            }
        });
    }

    private void login() {
        String pass = edUser.getText().toString();
        String user = edPass.getText().toString();
        Intent i = new Intent(this, TomarHoraMedicaActivity.class);
        if (user.equals("admin") && pass.equals("admin")) {
            startActivity(i);
        }else {
            if(user.equals("root") && pass.equals("root")){
                startActivity(i);
            }else{
                if(user.equals("morty") && pass.equals("morty")) {
                    startActivity(i);
                }else{
                    Toast.makeText(this, "Datos incorrectos",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
