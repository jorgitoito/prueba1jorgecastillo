package com.example.pipe.jorgecastillo_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Date;

public class TomarHoraMedicaActivity extends AppCompatActivity {

    private Button btnGuardar,btnLimpiar,btnListar,btnCerrar;
    private EditText edNombre,edFecha,edHora,edTelefono;
    private Date date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tomar_hora_medica);
        initView();
    }

    private void initView() {
        btnCerrar = (Button)findViewById(R.id.btnCerrarSesion);
        btnGuardar = (Button)findViewById(R.id.btnGuardar);
        btnLimpiar = (Button)findViewById(R.id.btnLimpiar);
        btnListar = (Button)findViewById(R.id.btnListar);
        edFecha = (EditText) findViewById(R.id.edFecha);
        edHora = (EditText)findViewById(R.id.edHora);
        edNombre = (EditText)findViewById(R.id.edNombre);
        edTelefono = (EditText)findViewById(R.id.edTelefono);
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiarTxt();
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(TomarHoraMedicaActivity.this, LoginPacienteActivity.class);
                startActivity(i);
            }
        });
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent x = new Intent(TomarHoraMedicaActivity.this, ListadoDeHorasTomadasActivity.class);
                startActivity(x);
            }
        });
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validarCampos()){
                    if(AdministrarHora.getInstance().guardarHora(obtenerPaciente())){
                        Toast.makeText(TomarHoraMedicaActivity.this, "Grabo dueño!", Toast.LENGTH_SHORT).show();
                        limpiarTxt();
                    } else {
                        Toast.makeText(TomarHoraMedicaActivity.this, "Existe este telefono", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private Hora obtenerPaciente() {
        return new Hora(getTextET(edFecha),getTextET(edNombre),getTextET(edTelefono),getTextET(edHora));
    }

    private String getTextET(EditText et) {
        return et.getText().toString();
    }
    private boolean validarCampos() {
        String nombre = edNombre.getText().toString();
        String fecha= edFecha.getText().toString();
        String hora= edHora.getText().toString();
        String telefono = edTelefono.getText().toString();

        if(nombre.isEmpty()){
            Toast.makeText(this, "Ingrese un nombre", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(fecha.isEmpty()){
            Toast.makeText(this, "Ingrese una fecha", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(hora.isEmpty()){
            Toast.makeText(this, "Ingrese una hora", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(telefono.isEmpty()){
            Toast.makeText(this, "Ingrese un telefono valido", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void limpiarTxt() {
        edFecha.setText("");
        edHora.setText("");
        edTelefono.setText("");
        edNombre.setText("");
    }
}
