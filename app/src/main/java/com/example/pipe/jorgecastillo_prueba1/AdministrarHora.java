package com.example.pipe.jorgecastillo_prueba1;

import java.util.ArrayList;

/**
 * Created by pipe on 23-09-17.
 */

public class AdministrarHora {

    private static AdministrarHora instance;
    private ArrayList<Hora> values;

    protected AdministrarHora() { values = new ArrayList<>(); }

    public static AdministrarHora getInstance() {
        if (instance == null) {
            instance = new AdministrarHora();
        }
        return instance;
    }

    public boolean guardarHora(Hora hora) {
        //if(!buscarHora(hora)) {
            values.add(hora);
            return true;
        //}else {
          //  return false;
        //}
    }

    private boolean buscarHora(Hora hora) {
        for (Hora a : values) {
            if (hora.equals(a)) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<Hora> listarHora() {
        return values;
    }
}
