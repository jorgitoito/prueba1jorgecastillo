package com.example.pipe.jorgecastillo_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class ListadoDeHorasTomadasActivity extends AppCompatActivity {

    private ListView lvHora;
    private ListadoDeHorasTomadasAdapter listadoDeHorasTomadasAdapter;
    private Button btnVolver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_de_horas_tomadas);
        lvHora = (ListView) findViewById(R.id.lbHoras);
        listadoDeHorasTomadasAdapter = new ListadoDeHorasTomadasAdapter(this, AdministrarHora.getInstance().listarHora());
        lvHora.setAdapter(listadoDeHorasTomadasAdapter);
        btnVolver = (Button)findViewById(R.id.btnVolverL);
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ListadoDeHorasTomadasActivity.this, TomarHoraMedicaActivity.class);
                startActivity(i);
            }
        });
    }
}
